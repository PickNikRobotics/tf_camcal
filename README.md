# TF CamCal

## Run

    cd ~/SOMETHING/calibration_recording
    rosrun tf_camcal tf_camcal

To specify an alternate image stream:
   rosrun tf_camcal tf_camcal _image:=/new/image/topic
   
Change the image topics and tf frames manually in the code.
