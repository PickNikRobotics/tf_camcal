
#include <ros/ros.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include <cstdio>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Transform.h>		
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>

using std::endl;
using std::cout;
//
#define DEFAULT_IMAGE_TOPIC "/xtion_right/rgb/image_mono"
#define DEFAULT_TF_DEST "jaco2_link_hand"


void writePGM(sensor_msgs::ImageConstPtr src, int serial, const char* comment)
{
  char fileName[32];
  sprintf(fileName, "image%05d.pgm", serial);
  FILE* outFile = fopen(fileName, "w");
  fprintf(outFile, "P2\n640 480\n255\n");
  fprintf(outFile, "# %s\n", comment);

  //make an image time comment out of the header
  double imageTime = src->header.stamp.sec + ((double)src->header.stamp.nsec/1e9);
  fprintf(outFile, "# %1.9f\n", imageTime);

  int i,j;

  //cout << "Step: " << src->step << endl;

  for (i =0; i<src->height; i++)
    {
      for (j=0; j<src->width; j++)
	{
	  fprintf(outFile, "%u ", src->data[i*src->width + j]);
	}
      fprintf(outFile, "\n");
    }

  fclose(outFile);
}

void writePGM_16(sensor_msgs::ImageConstPtr src, int serial, const char* comment)
{
  char fileName[32];
  sprintf(fileName, "image%05d.pgm", serial);
  FILE* outFile = fopen(fileName, "w");
  fprintf(outFile, "P2\n640 480\n255\n");
  fprintf(outFile, "# %s\n", comment);

  //make an image time comment out of the header
  double imageTime = src->header.stamp.sec + ((double)src->header.stamp.nsec/1e9);
  fprintf(outFile, "# %1.9f\n", imageTime);

  
  int i,j;

  cout << "Step: " << src->step << endl;
  uint16_t *srcBase = (uint16_t*) &src->data[0];
  uint16_t trimVal;
  for (i =0; i<src->height; i++)
    {
      for (j=0; j<src->width; j++)
	{
	  trimVal = ((float)(srcBase[i*src->width + j]) / 2048 * 400);
	  if (trimVal > 60)
	    trimVal = 250;
	  else
	    trimVal = 10;
	  fprintf(outFile, "%u ", trimVal );
	}
      fprintf(outFile, "\n");
    }

  fclose(outFile);
}

image_transport::ImageTransport *it;
image_transport::Subscriber subs;
sensor_msgs::ImageConstPtr freshImages;

pthread_mutex_t *topicLocks;
pthread_mutex_t topicBell;
pthread_cond_t newTopic;
ros::AsyncSpinner    *spinner;




void print_usage(const char *prog)
{
  printf("Usage: %s\n", prog);
  printf("  image: image topic to connect to (default: %s)\n", DEFAULT_IMAGE_TOPIC);
  printf("  tf_dest: image topic to connect to (default: %s)\n", DEFAULT_TF_DEST);
  exit(1);
}

void imageCallback(const sensor_msgs::Image::ConstPtr& msg)
{
  //cout << "Got image!" << endl;
  //Save the latest and greatest image
  freshImages = msg;
}

int main(int argc, char ** argv)
{
  //Initialize ROS
  ros::init(argc, argv, "tf_camcal", ros::init_options::AnonymousName);
  /* check command line arguments */

  spinner = new ros::AsyncSpinner(2);
  spinner->start();

  //world jaco2_link_hand
  ros::NodeHandle nh;

  double rate_hz;
  rate_hz = 1;
  // read rate parameter
  ros::NodeHandle p_nh("~");
  // p_nh.param("rate", rate_hz, 1);
  
  std::string sourceImage = std::string(DEFAULT_IMAGE_TOPIC);
  std::string source_frameid = std::string("world");
  std::string target_frameid = std::string(DEFAULT_TF_DEST);

  
  //p_nh.getParam("tf_frame", target_frameid);
  //p_nh.getParam("image", sourceImage);

  ros::Rate rate(20);

  //Instantiate a local transform listener
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tf(tfBuffer);


   // Wait for up to one second for the first transforms to become avaiable. 
  //tf.waitForTransform(source_frameid, target_frameid, ros::Time(), ros::Duration(1.0));

  //Nothing needs to be done except wait for a quit
  //The callbacks withing the listener class
  //will take care of everything

  //Log the transforms as they come in. Reap the latest image available and save
  it = new image_transport::ImageTransport(nh);

  ROS_INFO("Using image topic: [%s]", sourceImage.c_str());
  ROS_INFO("Using destination tf: [%s]", target_frameid.c_str());
  ROS_INFO("Writing to dir: [%s]", getenv("PWD"));
  subs = it->subscribe(sourceImage, 1000, imageCallback);

  int imageCount = 0;

  char comment[128];
  
  while(ros::ok())
    {

      try
      {        
	geometry_msgs::TransformStamped transformStamped;
        transformStamped = tfBuffer.lookupTransform(source_frameid, target_frameid, ros::Time(0));
        std::cout.precision(3);
        std::cout.setf(std::ios::fixed,std::ios::floatfield);
	
        //std::cout << "At time " << transformStamped.header.stamp.toSec() << std::endl;
        double yaw, pitch, roll;
	
        //echo_transform.getBasis().getRPY(roll, pitch, yaw);
	geometry_msgs::Quaternion q = transformStamped.transform.rotation;
	geometry_msgs::Vector3 v = transformStamped.transform.translation;
        // std::cout << "- Translation: [" << v.x << ", " << v.y << ", " << v.z << "]" << std::endl;
        // std::cout << "- Rotation: in Quaternion [" << q.x << ", " << q.y << ", " 
        //           << q.z << ", " << q.w << "]" << std::endl;

	//Convert to RPY (rad)

	tf2::Quaternion rot_q(q.x, q.y, q.z, q.w);
	tf2::Matrix3x3 rot_m(rot_q);
	rot_m.getRPY(roll, pitch, yaw);
	
	
	// std::cout << "            in RPY (radian) [" <<  roll << ", " << pitch << ", " << yaw << "]" << std::endl
        //           << "            in RPY (degree) [" <<  roll*180.0/M_PI << ", " << pitch*180.0/M_PI << ", " << yaw*180.0/M_PI << "]" << std::endl;

        //print transform
	double tfTime = transformStamped.header.stamp.sec + ((double)transformStamped.header.stamp.nsec)/1e9;
	
	//Save image to disk
	if (freshImages)
	  {
	    //cout << "Format: " << freshImages->encoding << endl;
	    //cout << "Writing image " << imageCount << endl;
	    sprintf(comment, "%d, %1.9f, %1.6f, %1.6f, %1.6f, %1.6f, %1.6f, %1.6f\n",
		    imageCount, tfTime, v.x, v.y, v.z, roll, pitch, yaw);

	    //sprintf(comment, "");
	    writePGM(freshImages, imageCount, comment);

	    imageCount++;
	  }
      }
      catch(tf2::TransformException& ex)
      {
        ROS_ERROR_STREAM_NAMED("tf_camcal","Failure at "<< ros::Time::now());
        std::cout << "Exception thrown:" << ex.what()<< std::endl;
        std::cout << "The current list of frames is:" <<std::endl;
        //std::cout << echoListener.tf.allFramesAsString()<<std::endl;
        
      }

      rate.sleep();
    }

  return 0;
};
